# PHP Challenge 2020

Bem vindo(a) ao nosso PHP Challenge, aqui você terá as informações nescessárias para o desenvolvimento do desafio.
Precisamos de pessoas com espírito de equipe e que ame desafios. Faça um fork do repositório e mão na massa. Não guarde as dúvidas, nos procure antes de seguir uma interpretação errado.  

Sucesso :)

---

# O Desafio

O nosso desafio, para que possamos avaliar suas habilidades e competências para a
função, você terá que desenvolver um programa para um mercado que permita o cadastro
dos produtos, dos tipos de cada produto, dos valores percentuais de imposto dos tipos de
produtos, a tela de venda, onde será informado os produtos e quantidades adquiridos, o
sistema deve apresentar o valor de cada item multiplicado pela quantidade adquirida e a
quantidade pago de imposto em cada item, um totalizador do valor da compra e um
totalizador do valor dos impostos.
O sistema deve ser desenvolvido utilizando as seguintes tecnologias:
* PHP
* Banco de dados Mysql

Quaisquer dúvidas sobre a especificação do programa a ser desenvolvido, podem
ser questionados.


## Critérios de Avaliação

O desafio será avaliado através dos seguintes critérios.

### Entrega

* O resultado final está completo para ser executado?
* O resultado final atende ao que se propõe fazer?
* O resultado final atende totalmente aos requisitos propostos?

### Boas Práticas

* O código está de acordo com as boas Práticas do PHP?
* O código está bem estruturado?
* O código está fluente na linguagem?
* O código faz o uso correto de _Design Patterns_?
* O código é fácil de compreender?

### Documentação

* O código foi entregue com um arquivo de README claro de como se guiar?
* O código possui comentários pertinentes?
* O código está em algum controle de versão?
* Os commits são pequenos e consistentes?
* As mensagens de commit são claras?



## IMPORTANTE: 

* Evitar o uso de framework PHP.
* Não esquecer de commitar seu projeto em um repositório seu, e claro deixa-lo público para que possamos avaliar.
* Ao finalizar, enviar o link do seu repositório para ismael@focomultimidia.com
